ARG BASE_REGISTRY="registry.access.redhat.com"
ARG BASE_IMAGE="ubi7/ubi"
ARG BASE_TAG="7.7"

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}
...
RUN mkdir /build_output 

COPY "${TARBALL}" /build_output/anchore-buildblob.tgz

RUN tar -xzv -C /build_output -f /build_output/anchore-buildblob.tgz
...
LABEL name=example-name

EXPOSE ${ANCHORE_SERVICE_PORT}
...
HEALTHCHECK --start-period=20s \
    CMD curl -f http://localhost:8228/health || exit 1
...
USER anchore:anchore
...
ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["anchore-manager", "service", "start", "--all"]
