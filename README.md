# Examples for OCI-compliant containers

## Base Image

Requirements for an approved set of base images for commercial and open source container images to use. 

| Standard base images      | Minimal base images |
| ----------- | ----------- |
| UBI7 & UBI8 | UBI7 & UBI8 Minimal |
| Alpine   | Minideb  |
| Debian | Distroless |

**Note:** With an onramp to utilization of base image with minimal attack surface area.

Each of the above container images should be build with a hardening script to strip out any unnecessary packages, files, etc. 

#### Example UBI7 image with hardening script
```
COPY scripts/dsop-fix*.sh /

RUN chmod +x /dsop-fix-{1,2}.sh && for i in $(ls /dsop-fix-{1,2}.sh); do sh ${i}; done
RUN yum update -y && rm -rf /var/cache/yum/ /var/tmp/* /tmp/* /var/tmp/.???* /tmp/.???*
RUN chmod +x /dsop-fix-3.sh && for i in $(ls /dsop-fix-3.sh); do sh ${i}; done && rm -rf /dsop-fix*.sh
```

- `dsop-alpine-fix.sh` - For alpine images
- `dsop-debian-fix.sh` - For debian images

**Note:** Policies should be created an applied to check for the proper script being copied into the image, and then executed with a `RUN` instruction.

## Vendor images

### Base image

The following instructions should be utilized for consistency when selecting a base image:

```dockerfile
ARG BASE_REGISTRY=registry.access.redhat.com
ARG BASE_IMAGE=ubi8/ubi
ARG BASE_TAG=8.0
```
**Note:** Individual policies should be created to check for the above `ARG` values. Additionally, a policy rule check should be created to enforce via the `FROM` instruction and should refer to the base images from the approved set above.

### Instructions

- `LABEL` - Clear definitions of name, release, version, summary, etc.
- `COPY` - Validate software artifacts are being properly copied into container image after being staged. 
- `ENV` - Limit the number of ENV variables. 
- `ARG` - Limit ARG values.
- `HEALTHCHECK` - HEALTHCHECK required. ex. Anchore: `--start-period=20s CMD curl -f http://localhost:8228/health || exit 1`
- `EXPOSE` - Only expose what is necessary for specific application. ex. Redis: `6379`
- `USER` - nonroot user should be defined. Should be application specific. ex. Anchore: `anchore:anchore`
- `RUN` - RUN instructions should be carefully checked to ensure best practices are being followed. Ex. `--nogpgcheck` should not be added. No external calls via `wget`, `curl`, etc. to places on the public internet. Regular expression matches for http, https, ftp, sftp.
- Application specific configuration files (ex. redis.conf) should be carefully validated against security requirements. No `ENV` variables including default users, passwords, etc. 

**Note:** For each of above requirements, specific policies should be created per application image (ex. Redis policy rules). 

#### Redis Image

**Base image:** UBI7-stigd

#### Applicable policies

- UBI7 policy rules
- UBI7 whitelist
- Redis policy rules
- Redis whitelist

### Dockerfile to change

```dockerfile
### Should not store Nexus repository credentials as ARGS (will should up in image history)
ARG NEXUS_SERVER=nexus.52.61.140.4.nip.io
ARG NEXUS_USERNAME
ARG NEXUS_PASSWORD

FROM registry.access.redhat.com/ubi8/ubi:8.0
### No LABELS
# Update the base image
RUN yum -y update && yum clean all

### Should not fetch software artifacts in Dockerfile logic (should be staged)
RUN curl -k -fu ${NEXUS_USERNAME}:${NEXUS_PASSWORD} https://${NEXUS_SERVER}/repository/dsop/snort/libdnet-1.12-0.13.1.el7.x86_64.rpm -o /tmp/libdnet-1.12-0.13.1.el7.x86_64.rpm && \
    yum -y --nogpgcheck --disableplugin=subscription-manager localinstall /tmp/libdnet-1.12-0.13.1.el7.x86_64.rpm && \
    yum clean all
RUN curl -k -fu ${NEXUS_USERNAME}:${NEXUS_PASSWORD} https://${NEXUS_SERVER}/repository/dsop/snort/daq-2.0.6-1.el7.x86_64.rpm -o /tmp/daq-2.0.6-1.el7.x86_64.rpm && \
    yum -y --nogpgcheck --disableplugin=subscription-manager localinstall /tmp/daq-2.0.6-1.el7.x86_64.rpm && \
    yum clean all
RUN curl -k -fu ${NEXUS_USERNAME}:${NEXUS_PASSWORD} https://${NEXUS_SERVER}/repository/dsop/snort/snort-2.9.15.1-1.f29.x86_64.rpm -o /tmp/snort-2.9.15.1-1.f29.x86_64.rpm && \
    yum -y --nogpgcheck --disableplugin=subscription-manager localinstall /tmp/snort-2.9.15.1-1.f29.x86_64.rpm && \
    yum clean all

# See https://unix.stackexchange.com/questions/209813/libdnet-is-installed-but-cant-be-found-by-snort
RUN ln -s /usr/lib64/libdnet.so.1.0.1 /usr/lib64/libdnet.1

### No root user
USER root
### No Healthcheck
RUN snort --version
```

### Valid Dockerfile

```dockerfile
ARG BASE_REGISTRY="registry.access.redhat.com"
ARG BASE_IMAGE="ubi7/ubi"
ARG BASE_TAG="7.7"

######## This is stage1 where anchore wheels, binary deps, and any items from the source tree get staged to /build_output ########
FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG} as anchore-engine-builder

ARG ARTIFACT_PROJECT="anchore-engine"
ARG ARTIFACT_VERSION="v0.6.1"
ARG TARBALL="${ARTIFACT_PROJECT}"-"${ARTIFACT_VERSION}"-buildblob.tgz 

RUN mkdir /build_output 
### Copy artifacts which have been staged
COPY "${TARBALL}" /build_output/anchore-buildblob.tgz

RUN tar -xzv -C /build_output -f /build_output/anchore-buildblob.tgz
...
# Build setup section

######## This is stage2 which does setup and install entirely from items from stage1's /build_output ########
FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG} as anchore-engine-final
...
# Copy skopeo artifacts from build step
COPY --from=anchore-engine-builder /build_output /build_output
...
# Container metadata section
### Include LABELS
LABEL anchore_cli_commit=$CLI_COMMIT \
      anchore_commit=$ANCHORE_COMMIT \
      name="anchore-engine" \
      maintainer="dev@anchore.com" \
      vendor="Anchore Inc." \
      version=$ANCHORE_ENGINE_VERSION \
      release=$ANCHORE_ENGINE_RELEASE \
      summary="Anchore Engine - container image scanning service for policy-based security, best-practice and compliance enforcement." \
      description="Anchore is an open platform for container security and compliance that allows developers, operations, and security teams to discover, analyze, and certify container images on-premises or in the cloud. Anchore Engine is the on-prem, OSS, API accessible service that allows ops and developers to perform detailed analysis, run queries, produce reports and define policies on container images that can be used in CI/CD pipelines to ensure that only containers that meet your organization’s requirements are deployed into production."

...
### Healthcheck added
HEALTHCHECK --start-period=20s \
    CMD curl -f http://localhost:8228/health || exit 1
## Non-root user added
USER anchore:anchore

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["anchore-manager", "service", "start", "--all"]
```
